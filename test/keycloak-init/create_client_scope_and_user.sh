#!/usr/bin/env sh

# check if keycloak is reachable
echo "waiting for keycloak"
until $(curl --output /dev/null --silent --head --fail "${KEYCLOAK_ENDPOINT}"); do
  printf '.'
  sleep 3
done

# authenticate
kcadm config credentials \
  --server "${KEYCLOAK_ENDPOINT}" \
  --realm "${KEYCLOAK_REALM}" \
  --user "${KEYCLOAK_USER}" \
  --password "${KEYCLOAK_PASS}"

# Create realm
#realm_name=$(jq -r '.id' < "realm.json")
#
#if test ! $(kcadm get "realms/${realm_name}"); then
#  kcadm create realms -f "realm.json"
#fi

# create sclient-scope
client_scope_name=$(cat client-scope.json | jq -r .name)
client_scope_id=$(kcadm get client-scopes \
  | jq -r --arg client_scope_name "${client_scope_name}" '.[] | select(.name==$client_scope_name).id')
if test -z "${client_scope_id}"; then
  echo "creating client scope ${client_scope_name} in ${KEYCLOAK_REALM} on ${KEYCLOAK_ENDPOINT}.."
  kcadm create client-scopes --realm "${KEYCLOAK_REALM}" -f  "client-scope.json"
fi

# create user
user_name=$(cat test-user.json | jq -r .username)
user_search_result=$(kcadm get users -q username=test-user)
if test "${user_search_result}" = "[ ]"; then
  echo "create user ${user_name} in ${KEYCLOAK_REALM} on ${KEYCLOAK_ENDPOINT}.."
  kcadm create users -f test-user.json
fi

kcadm set-password --username "${user_name}" --new-password "${user_name}"
