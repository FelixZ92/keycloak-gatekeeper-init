#!/usr/bin/env sh

if test ! -f "${KEYCLOAK_CLIENT_DEFINITION_FILE}"; then
  echo "no client definition given"
  exit 1
fi

# check if keycloak is reachable
echo "waiting for keycloak"
until $(curl --output /dev/null --silent --head --fail "${KEYCLOAK_ENDPOINT}"); do
  printf '.'
  sleep 3
done

# authenticate
kcadm config credentials --server ${KEYCLOAK_ENDPOINT} --realm ${KEYCLOAK_REALM} --user ${KEYCLOAK_USER} --password ${KEYCLOAK_PASS}

if test ${?} -ne 0; then
 echo "authentication failed, exiting.."
 exit 1
fi 

# validate client scopes
scopes=$(cat "${KEYCLOAK_CLIENT_DEFINITION_FILE}" | jq -r '.defaultClientScopes | .[]')
echo "validate scopes... ${scopes}"
for scope in ${scopes}; do
  echo "validate scope ${scope}.."
  scope_id=$(kcadm get client-scopes \
  | jq -r --arg client_scope_name "${scope}" '.[] | select(.name==$client_scope_name).id')

  if test -z "${scope_id}"; then
    echo "client scope ${scope} not found in realm ${KEYCLOAK_REALM}, exiting.."
    exit 1
  fi
done

# Create or update client
client_name=$(jq -r .clientId < "${KEYCLOAK_CLIENT_DEFINITION_FILE}")
export KEYCLOAK_CLIENT_ID="${client_name}"

client_id=$(kcadm get clients \
  | jq -r --arg client_name "${client_name}" '.[] | select(.clientId==$client_name).id')

if test -z "${client_id}"; then
  echo "creating client ${client_name} in ${KEYCLOAK_REALM} on ${KEYCLOAK_ENDPOINT}.."
  kcadm create clients -f "${KEYCLOAK_CLIENT_DEFINITION_FILE}" -s secret="${KEYCLOAK_CLIENT_SECRET}"
  client_id=$(kcadm get clients \
  | jq -r --arg client_name "${client_name}" '.[] | select(.clientId==$client_name).id')
else
  echo "updating client ${client_name} with id ${client_id} in ${KEYCLOAK_REALM} on ${KEYCLOAK_ENDPOINT}.."
  kcadm update "clients/${client_id}" -f "${KEYCLOAK_CLIENT_DEFINITION_FILE}" -s secret="${KEYCLOAK_CLIENT_SECRET}"
fi

# Add protocol mapper
if test -n "${KEYCLOAK_CLIENT_SCOPE}" && test -f "${KEYCLOAK_PROTOCOL_MAPPER_DEFINITION_FILE}" ; then
  protocol_mapper_name=$(jq -r .name < "${KEYCLOAK_PROTOCOL_MAPPER_DEFINITION_FILE}")
  client_scope_id=$(kcadm get client-scopes \
  | jq -r --arg client_scope_name "${KEYCLOAK_CLIENT_SCOPE}" '.[] | select(.name==$client_scope_name).id')

  protocol_mapper_id=$(kcadm get "client-scopes/${client_scope_id}/protocol-mappers/models" \
  | jq -r --arg protocol_mapper_name "${protocol_mapper_name}" '.[] | select(.name==$protocol_mapper_name).id')

  if test -z "${protocol_mapper_id}"; then
    echo "creating protocol mapper ${protocol_mapper_name} for client-scope ${KEYCLOAK_CLIENT_SCOPE} in ${KEYCLOAK_REALM} on ${KEYCLOAK_ENDPOINT}.."
    kcadm create "client-scopes/${client_scope_id}/protocol-mappers/models" -f "${KEYCLOAK_PROTOCOL_MAPPER_DEFINITION_FILE}"
  fi
fi

export KEYCLOAK_CLIENT_SECRET=$(kcadm get clients/"${client_id}"/client-secret | jq -r .value)

find "${GATEKEEPER_ADDITIONAL_CONFIG_DIR}" -name "*.yaml"  | xargs yq m -i /keycloak/config-template.yaml

envsubst < /keycloak/config-template.yaml > "${GATEKEEPER_CONFIG_FILE}"

cat ${GATEKEEPER_CONFIG_FILE}

rm -rf "${HOME}"/.keycloak
